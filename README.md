# Earth View by Google 
### https://earthview.withgoogle.com
Earth View is a collection of the most beautiful and striking landscapes found in Google Earth.

JSON format: (https://gitlab.com/j_hardy/earthview/-/raw/main/earthview.json)